def Chiffrage(phrase):
    taillechaine = len(phrase)
    chiffra = ""
    for cpt in range(taillechaine) :
        if ( ord(phrase[cpt])<= ord('z')  and  ord(phrase[cpt]) >= ord('a') )  or ( ord(phrase[cpt])<= ord('Z') and ord(phrase[cpt]) >= ord('A') ) :
            if ( ( ord(phrase[cpt]) + 8 < ord('a') and ord(phrase[cpt]) + 8 > ord('Z') ) or ( ord(phrase[cpt]) + 8 > ord('z') ) ) :
                chiffra += chr( ord(phrase[cpt]) - 18 )
            else :
                chiffra += chr( ord(phrase[cpt]) + 8 )
        else :
            chiffra += phrase[cpt]

    return chiffra

def Dechiffrage(phrase):
    chiffra=""
    taillechaine = len(phrase)
    for cpt in range (taillechaine) :
        if ( ord(phrase[cpt]) <= ord('z')  and  ord(phrase[cpt]) >= ord('a') )  or ( ord(phrase[cpt])<= ord('Z') and ord(phrase[cpt]) >= ord('A') ) :
            if ( (  ord(phrase[cpt]) - 8 < ord('a') and ord(phrase[cpt]) -8  > ord('Z') ) or ( ord(phrase[cpt]) -8 < ord('A') ) ) :
                chiffra += chr( ord(phrase[cpt]) + 18)
            else :
                chiffra += chr( ord(phrase[cpt]) - 8 )
        else :
            chiffra += phrase[cpt]

    return chiffra