from ChiffreDeCesar import *
import unittest


class TestChiffrageFunctions (unittest.TestCase):

    def test_Chiffrage(self):
        self.assertEqual(Chiffrage("a"), "i")
        self.assertEqual(Chiffrage("z"), "h")
        self.assertEqual(Chiffrage("Hello World"), "Pmttw Ewztl")

    def test_Dechiffrage(self):
        self.assertEqual(Dechiffrage("i"), "a")
        self.assertEqual(Dechiffrage("h"), "z")
        self.assertEqual(Dechiffrage("Pmttw Ewztl"), "Hello World")