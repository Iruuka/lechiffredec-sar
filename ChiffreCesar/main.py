from tkinter import *
from chiffrecesar import *

def button_chiffrement_callback():
    val_chifr.set(Chiffrage(value.get(), decalage.get()))

def button_dechiffrement_callback():
    val_dechifr.set(Dechiffrage(value.get(), decalage.get()))


frame = Tk()

decalage = IntVar()
decalage.set(0)

value = StringVar()
value.set("Val par def")

val_chifr = StringVar()
val_chifr.set("Default")

val_dechifr = StringVar()
val_dechifr.set("Default")


label_affiche = Label(frame, text="insérer phrase")
label_decalage = Label(frame, text="Choisissez votre Décalage")
label_chiffr = Label(frame, textvariable= val_chifr)
label_dechiffr = Label(frame, textvariable= val_dechifr)

entree=Entry(frame, textvariable = value, width=30)
decal=Entry(frame, textvariable = decalage, width=30)

bouton_chiffrement = Button(frame, text="Chiffrement", command=button_chiffrement_callback)
bouton_dechiffrement = Button(frame, text="Dechiffrement", command=button_dechiffrement_callback)

label_affiche.pack()
entree.pack()

label_decalage.pack()
decal.pack()

bouton_chiffrement.pack()
label_chiffr.pack()

bouton_dechiffrement.pack()
label_dechiffr.pack()

frame.mainloop()