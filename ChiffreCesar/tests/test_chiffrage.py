import unittest
from ChiffreCesar import *

class TestChiffrageFunctions (unittest.TestCase):

    def test_Chiffrage(self):
        self.assertEqual(Chiffrage("a",13), "n")
        self.assertEqual(Chiffrage("z",13), "m")
        self.assertEqual(Chiffrage("Hello World",13), "Uryyb Jbeyq")

    def test_Dechiffrage(self):
        self.assertEqual(Dechiffrage("n",13), "a")
        self.assertEqual(Dechiffrage("m",13), "z")
        self.assertEqual(Dechiffrage("Uryyb Jbeyq",13), "Hello World")
