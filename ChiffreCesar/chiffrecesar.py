def Chiffrage(phrase, decalage):
    taillechaine = len(phrase)
    chiffra = ""
    for cpt in range(taillechaine) :
        if ( ord(phrase[cpt])<= ord('z')  and  ord(phrase[cpt]) >= ord('a') )  or ( ord(phrase[cpt])<= ord('Z') and ord(phrase[cpt]) >= ord('A') ) :
            if ( ( ord(phrase[cpt])+decalage > ord('z') and ord(phrase[cpt]) <= ord('z')) or ( ord(phrase[cpt])+decalage > ord('Z') and ord(phrase[cpt]) <= ord('Z'))) :
                chiffra += chr( ord(phrase[cpt]) - 26 + decalage)
            else :
                chiffra += chr( ord(phrase[cpt]) + decalage)
        else :
            chiffra += phrase[cpt]
    return chiffra

def Dechiffrage(phrase, decalage):
    chiffra=""
    taillechaine = len(phrase)
    for cpt in range (taillechaine) :
        if ( ord(phrase[cpt]) <= ord('z')  and  ord(phrase[cpt]) >= ord('a') )  or ( ord(phrase[cpt])<= ord('Z') and ord(phrase[cpt]) >= ord('A') ) :
            if (( ord(phrase[cpt]) < ord('a')+decalage and ord(phrase[cpt]) >= ord('a')) or ( ord(phrase[cpt]) < ord('A')+decalage and ord(phrase[cpt]) >= ord('A'))) :
                chiffra += chr( ord(phrase[cpt]) - decalage +26)
            else :
                chiffra += chr( ord(phrase[cpt]) - decalage)
        else :
            chiffra += phrase[cpt]
    return chiffra
